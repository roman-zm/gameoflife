#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QTimer>
#include <QVector>

class Game : public QObject
{
    Q_OBJECT
public:
    explicit Game(QObject *parent = nullptr);

public slots:
    void aliveCell(int x, int y);
    void deadCell(int x, int y);
    void sizeChanged(int size);

    void startGame();
    void stopGame();

signals:
    void livingCells(QVector< QPair<int, int> > living);

    void gameStarted();
    void gameStopped();

private slots:
    void nextTurn();

private:
    enum class CellState{Dead = 0, Alive = 1};

    CellState cellIsAlive(int x, int y);

    QVector< QVector<CellState> > cellsTable;

    QVector< QPair<int, int> > living;
    QVector< QPair<int, int> > prevLiving;
    QTimer* timer;

    int _size;

};

#endif // GAME_H
