#ifndef GAMETABLE_H
#define GAMETABLE_H

#include <QWidget>
#include <QPoint>

namespace Ui {
class GameTable;
}

class GameTable : public QWidget
{
    Q_OBJECT

public:
    explicit GameTable(int _size, QWidget *parent = 0);
    ~GameTable();

    int size() const;
    void setSize(int size);

signals:
    void aliveCell(int x, int y);
    void deadCell(int x, int y);
    void sizeChanged(int size);

public slots:
    void livingCellsSlot(QVector< QPair<int, int> > recievedLiving);

private slots:
    void on_tableWidget_cellClicked(int row, int column);

private:
    void init();
    void loadCells();
    void switchCell(int row, int column);

    Ui::GameTable *ui;
    int _size;

    QVector< QPair<int, int> > living, nextLiving;
};

#endif // GAMETABLE_H
