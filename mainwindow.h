#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>

#include "gametable.h"
#include "game.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setSize(int value);

private slots:
    void on_minusButton_clicked();
    void on_plusButton_clicked();
    void on_sizeSpinBox_valueChanged(int arg1);
    void on_playPauseButton_clicked();
    void on_stopButton_clicked();

    void gameStopped();
    void gameStarted();

private:
    Ui::MainWindow *ui;
    int size;
    GameTable* gameTable;
    Game* game;
};

#endif // MAINWINDOW_H
