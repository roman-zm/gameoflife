#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow),
    size(10),
    gameTable(new GameTable(size, this)),
    game(new Game(this))
{
    ui->setupUi(this);
    ui->verticalLayout->insertWidget(0, gameTable);

    game->sizeChanged(size);

    connect(gameTable, &GameTable::aliveCell, game, &Game::aliveCell);
    connect(gameTable, &GameTable::deadCell,  game, &Game::deadCell);
    connect(gameTable, &GameTable::sizeChanged, game, &Game::sizeChanged);

    connect(game, &Game::livingCells, gameTable, &GameTable::livingCellsSlot);

    connect(game, &Game::gameStarted, this, &MainWindow::gameStarted);
    connect(game, &Game::gameStopped, this, &MainWindow::gameStopped);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_minusButton_clicked()
{
    ui->sizeSpinBox->setValue(ui->sizeSpinBox->value() - 1);
}

void MainWindow::setSize(int value)
{
    gameTable->setSize(value);
}

void MainWindow::on_plusButton_clicked()
{
    ui->sizeSpinBox->setValue(ui->sizeSpinBox->value() + 1);
}

void MainWindow::on_sizeSpinBox_valueChanged(int arg1)
{
    setSize(arg1);
}

void MainWindow::on_playPauseButton_clicked()
{
    game->startGame();
}

void MainWindow::on_stopButton_clicked()
{
    game->stopGame();
}

void MainWindow::gameStopped()
{
    ui->stopButton->setDisabled(true);
    ui->playPauseButton->setDisabled(false);
}

void MainWindow::gameStarted()
{
    ui->stopButton->setDisabled(false);
    ui->playPauseButton->setDisabled(true);
}
