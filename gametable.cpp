#include "gametable.h"
#include "ui_gametable.h"

GameTable::GameTable(int _size, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameTable), _size(_size)
{
    ui->setupUi(this);
    this->setSize(_size);

    init();
}

GameTable::~GameTable()
{
    delete ui;
}

int GameTable::size() const
{
    return _size;
}

void GameTable::setSize(int size)
{
    _size = size;
    ui->tableWidget->setRowCount(size);
    ui->tableWidget->setColumnCount(size);

    emit sizeChanged(_size);

    loadCells();
}

void GameTable::livingCellsSlot(QVector<QPair<int, int> > recievedLiving)
{
    living = recievedLiving;
    loadCells();
}

void GameTable::init()
{
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->hide();

    loadCells();
}

void GameTable::loadCells()
{
    ui->tableWidget->clear();

    for(int i=0; i<this->_size; i++) {
        for(int j=0; j<this->_size; j++) {
            auto item = new QTableWidgetItem();
            item->setBackgroundColor(Qt::white);

            ui->tableWidget->setItem(i, j, item);
        }
    }

    foreach(auto cell, living) {
        int x = cell.first;
        int y = cell.second;

        ui->tableWidget->item(x, y)->setBackgroundColor(Qt::blue);
    }
}

void GameTable::switchCell(int row, int column)
{
    auto item = ui->tableWidget->item(row, column);

    if(item->backgroundColor() == Qt::white) {
        item->setBackgroundColor(Qt::blue);
        emit aliveCell(row, column);
    } else {
        item->setBackgroundColor(Qt::white);
        emit deadCell(row, column);
    }
}

void GameTable::on_tableWidget_cellClicked(int row, int column)
{
    this->switchCell(row, column);
}
