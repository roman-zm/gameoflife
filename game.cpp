#include "game.h"

Game::Game(QObject *parent) : QObject(parent),
    timer(new QTimer(parent))
{
    timer->setInterval(500);
    connect(timer, &QTimer::timeout, this, &Game::nextTurn);
}

void Game::aliveCell(int x, int y)
{
    cellsTable[x][y] = CellState::Alive;
    living.append(QPair<int, int>{x, y});
}

void Game::deadCell(int x, int y)
{
    cellsTable[x][y] = CellState::Dead;
    living.removeAll(QPair<int, int>{x, y});
}

void Game::sizeChanged(int size)
{
    _size = size;
    
    cellsTable.resize(_size);
    for(int i=0; i<_size; i++) {
        cellsTable[i].resize(_size);
    }

    QPair<int, int> cell;
    foreach (cell, living) {
        int x{cell.first}, y{cell.second};
        if(x < _size && y < _size)
            cellsTable[x][y] = CellState::Alive;
    }

    emit livingCells(living);
}

void Game::startGame()
{
    timer->start();
    emit gameStarted();
}

void Game::stopGame()
{
    timer->stop();
    emit gameStopped();
}

void Game::nextTurn()
{
    living.clear();
    QVector< QVector<CellState> > nextStepTable;
    nextStepTable.resize(_size);

    for(int i=0; i<_size; i++){
        nextStepTable[i].resize(_size);

        for(int j=0; j<_size; j++) {
            nextStepTable[i][j] = cellIsAlive(i, j);

            if(nextStepTable[i][j] == CellState::Alive) {
                living.append(QPair<int, int>(i, j));
            }
        }
    }

    cellsTable = nextStepTable;
    if(living == prevLiving) {
        stopGame();
    } else {
        prevLiving = living;
        emit livingCells(living);
    }
}

Game::CellState Game::cellIsAlive(int x, int y)
{
    int livingNeighbors{0};

    for(int i=x-1; i<=x+1; i++){
        for(int j=y-1; j<=y+1; j++){
            if(x!=i || y!=j) {
                int tempI{(_size + i) % _size};
                int tempJ{(_size + j) % _size};
                if(cellsTable[tempI][tempJ] == CellState::Alive)
                    livingNeighbors++;
            }
        }
    }

    if(cellsTable[x][y] == CellState::Alive) {
        if(livingNeighbors == 2 || livingNeighbors == 3)
            return CellState::Alive;
        else
            return CellState::Dead;
    } else {
        if(livingNeighbors == 3)
            return CellState::Alive;
        else
            return CellState::Dead;
    }
}
