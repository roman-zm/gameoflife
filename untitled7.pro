#-------------------------------------------------
#
# Project created by QtCreator 2018-05-22T18:52:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled7
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    gametable.cpp \
    game.cpp

HEADERS  += mainwindow.h \
    gametable.h \
    game.h

FORMS    += mainwindow.ui \
    gametable.ui
